﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PokeMod {
  public partial class MainForm : Form {
    
    private OpenFileDialog openBox;
    private Game juego;
        
    private string[] pkmnames;
    
    public MainForm() {
      openBox = new OpenFileDialog();
      openBox.Filter = "Pokemon Red/Blue Game|*.gb";

      InitializeComponent();
    }

    private void openGame(object sender, EventArgs e) {
      // Trata de abrir el archivo del juego
      try {
        // Mostrar el cuadro de diálogo y esperar
        // a ver si seleccionan el archivo del juego
        if (openBox.ShowDialog() != DialogResult.OK) {
          return;
        }

        // Checar el tamaño del archivo
        this.juego = new Game(openBox.FileName);

        // Preparar el cuadro de propiedades
        gridProp.SelectedObject = this.juego;
                
        // Enlistar todos los pokemons
        string[] nombres = this.juego.getPokemonNames();
        this.pkmnames = nombres;
        
        for( int i = 0; i < nombres.Length; i++ ) {
          // Agregar el ID del pokemon
          ListViewItem item = lvwNombres.Items.Add( (i+1).ToString() );
          item.Tag = i;
          // También el nombre que tiene
          item.SubItems.Add( nombres[i] );
          // Y el nombre clave, basándonos en el enumerador de Pokemons
          item.SubItems.Add( ((Game.PokemonEnum)(i+1)).ToString() );
        }
        
        // Enlistar todos los ataques
        string[] ataques = this.juego.extractAttacknames();
        for( int i = 0; i < ataques.Length; i++ ) {
          if( String.IsNullOrEmpty(ataques[i]) )
            continue;
          ListViewItem item = lvwAtaques.Items.Add( (i+1).ToString() );
          item.Tag = i;
          item.SubItems.Add( ataques[i] );
          // testeo para sacar la tabla de wiki
          txtTest.Text += "|| " + i.ToString()
            + " || 0x" + (i<16?"0":"")
            + (i+1).ToString("x").ToUpper()
            + " || " + ataques[i] + " ||" + "\r\n" ;
        }
                
        // cargar la evolución de charmander
        loadPokemonList(cbxCharmander,this.juego.GetBytes()[0x3B93A]);
        tbxCharmander.Text = this.juego.GetBytes()[0x3B939].ToString();
                
        //string[] cadenas = this.juego.extractStrings();
        //MessageBox.Show( cadenas.Length.ToString() );
        //foreach( string c in cadenas )
        	//txtTest.AppendText( c + "\r\n" + "-----------------" + "\r\n" );
        //MessageBox.Show( this.juego.extractText(0x881E5) );
      } catch (Exception ex) {
        // Mostrar el mensaje recibido
        MessageBox.Show(ex.Message);
      } finally {
        // Activar el botón de guardar si la
        // variable juego no es nula
        buttonCloseGame.Enabled
          = gridProp.Enabled
            = buttonSave.Enabled
              = juego != null;
      }
    }
        
    private void loadPokemonList(ComboBox cbx, byte pkmn)  {
    	cbx.Items.Clear();
    	cbx.SuspendLayout();
    	for( int i = 0; i < 100; i++ )
    		cbx.Items.Add(this.pkmnames[i]);
    	cbx.SelectedIndex = pkmn-1;
    	cbx.ResumeLayout();
    }

    private void saveGame(object sender, EventArgs e) {
      try {
        // Por obvias razones el juego debe haberse
        // iniciado previamente seleccionando el archivo
        if (juego == null) {
          throw new Exception("No se ha abierto ningún juego.");
        }
                
        // Cambiar los datos de charmander
        byte level;
        byte.TryParse(tbxCharmander.Text,out level);
        this.juego.changeEvolution("charmander",level,(byte)(cbxCharmander.SelectedIndex+1));
                
        // También pasamos los nombres de los Pokemon
        foreach(ListViewItem item in lvwNombres.Items) {
          int id = (int) item.Tag;
          this.juego.setPokemonName(id, item.SubItems[1].Text);
          item.BackColor = SystemColors.Window;
        }
                
                // Guardar los bytes del juego a un archivo
                File.WriteAllBytes(openBox.FileName, juego.GetBytes());

                // Si se guardó correctamente, mostramos un
                // mensaje de que todo salió muy bien
                MessageBox.Show("Se guardaron los cambios, ahora ve a jugar y comprueba los cambios.","Bien",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
			
        }

        void showPokemon(object sender, EventArgs e)
        {
        	if( lvwNombres.SelectedItems.Count == 1 )
        	{
        		ListViewItem item = lvwNombres.SelectedItems[0];
        		// Primero ponemos el nombre del pokemon
        		txtNombre.Text = item.SubItems[1].Text;
        		
        		gbxPokemon.Enabled = true;
        	}
        	else
        	{
        		gbxPokemon.Enabled = false;
        	}
        }
        
        void GuardarNombre(object sender, EventArgs e)
        {
        	if( lvwNombres.SelectedItems.Count == 1 )
        	{
        		ListViewItem item = lvwNombres.SelectedItems[0];
        		item.SubItems[1].Text = txtNombre.Text;
        		
        		// Para poder identificar que se cambió el nombre,
        		// vamos a poner un fondo verde.
        		item.BackColor = Color.Yellow;
        	}
        }
        
        void showHelp(object sender, EventArgs e) {
        	(new AboutForm()).ShowDialog();
        }
        
        void Button3Click(object sender, EventArgs e)
        {
    		int ancho = 16;
    		int alto = 5;
    		int posicion = 0xd323;
    	    int x = 0, y = 0;
    	    Graphics g = panel.CreateGraphics();
    	    byte[] bs;
    	    for( int i = 0; i < alto; i++ ) {
    	    	bs = juego.getSegment(posicion + alto*i,ancho);
    	    	foreach( byte b in bs ) {
    	    		MessageBox.Show("Cómete el pan: " + b.ToString());
    	    		g.DrawRectangle(new Pen(Color.FromArgb(b,b,b)),new Rectangle(x,y,1,1));
    	    		x += 1;
    	    	}
    	    	x = 0;
    	    	y += 1;
    	    }
        }
       
    }
}
