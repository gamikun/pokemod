﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Alignment = System.Drawing.ContentAlignment;
using System.ComponentModel;

namespace PokeMod {
  public class SimpleButton : Control {
   
    private bool
      _isMouseIn = false,
      _isMouseDown = false;
    
    public SimpleButton() : base() {
      base.Font = new Font("Century Gothic", 15);
      this.BackColor = Color.SlateGray;
      this.HoverColor = Color.SlateBlue;
      this.ForeColor = Color.White;
    }
                 
    protected override void OnMouseDown(MouseEventArgs e) {
      base.OnMouseDown(e);
      this._isMouseDown = true;
      this.Refresh();
    }
      
    protected override void OnMouseUp(MouseEventArgs e) {
      base.OnMouseUp(e);
      this._isMouseDown = false;
      this.Refresh();
    }
     
    protected override void OnPaint(PaintEventArgs args) {
      Graphics gf = base.CreateGraphics();
      SizeF textSize = gf.MeasureString(this.Text, this.Font);
      Color color;
      Brush brushFont = new SolidBrush(this.ForeColor);
      float
        x = this.Width / 2 - textSize.Width / 2,
        y = this.Height / 2 - textSize.Height / 2;
 
      if(this.Enabled == false) {
        color = Color.Gray;
        brushFont = new SolidBrush(Color.LightGray);
      } else if(this._isMouseDown == true) {
        color = this.DownColor;
      } else if(this._isMouseIn == true) {
        color = this.HoverColor;
      } else {
        color = this.BackColor;
      }
            
      gf.Clear(color);
      gf.DrawString(this.Text, this.Font, brushFont, x, y);
    }
    
    protected override void OnMouseEnter(EventArgs e) {
      base.OnMouseEnter(e);
      this._isMouseIn = true;
      this.Refresh();
    }
    
    protected override void OnGotFocus(EventArgs e) {
      base.OnGotFocus(e);
      this.Refresh();
    }
    
    protected override void OnMouseLeave(EventArgs e) {
      base.OnMouseLeave(e);
      this._isMouseIn = false;
      this.Refresh();
    }

    [Category("Appereance")]
    public Color HoverColor { get; set; }
    
    [Category("Appereance")]
    public Color DownColor { get; set; }
 
  }
}
