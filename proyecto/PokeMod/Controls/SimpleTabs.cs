﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;
using System.Diagnostics;

namespace PokeMod.Controls{
  public class SimpleTabs : TabControl {
    
    public SimpleTabs() : base() {
      base.DrawMode = TabDrawMode.OwnerDrawFixed;
      base.Font = new Font("Century Gothic", 13);
      this.TabBackColor = Color.Gray;
      this.BackColor = Color.DarkGray;
      this.TabForeColor = Color.White;
      base.ResizeRedraw = true;
    }
    
    protected override void OnCreateControl() {
    base.SetStyle(ControlStyles.UserPaint, true);
      base.OnCreateControl();
      
    }
    
    protected override void OnControlAdded(ControlEventArgs e) {
    e.Control.SetBounds(0,0,100,100);
    e.Control.Padding = new Padding(0);
    e.Control.Margin = new Padding(0);
      base.OnControlAdded(e);
    }

    protected override void OnDrawItem(DrawItemEventArgs e) {
      //base.OnDrawItem(e);
      //e.Graphics.Clear(Color.Transparent);
      //e.Graphics.FillRectangle(new SolidBrush(Color.Blue), 0, 0, 10, 10);
      //MessageBox.Show("hola");
    }
    
    protected override void OnPaint(PaintEventArgs e) {
      base.SuspendLayout();
      Graphics gf = e.Graphics;
      gf.Clear(this.BackgroundColor);
      int nControls = this.TabPages.Count;
      for(Int32 i = 0; i < nControls; i++) {
        TabPage tab = base.TabPages[i];
        Rectangle rect = this.GetTabRect(i);
        SizeF textSize = gf.MeasureString(tab.Text, this.Font);
        float x = rect.Width / 2 - textSize.Width / 2 + rect.X;
        float y = (rect.Height+2) / 2 - textSize.Height / 2;
        SolidBrush back;
        if(this.SelectedIndex == i) {
          back = new SolidBrush(this.SelectedColor);
        } else {
          back = new SolidBrush(this.TabBackColor);
        }
        gf.FillRectangle(back, rect.X+(i==0?2:0), 0, rect.Width+(i==0?-2:0), rect.Height + 4);
        gf.DrawString(tab.Text, base.Font, new SolidBrush(this.TabForeColor), x , y);
      }
      base.ResumeLayout();
    }
  
  protected override void OnMouseClick(MouseEventArgs e)
  {
    //base.OnMouseClick(e);
  }
  
    protected override void OnClick(EventArgs e) {
      return;
    }
    
    public Color TabBackColor { get; set; }
    
    [Browsable(true), Category("Appereance")]
    public Color BackgroundColor { get; set; }
    public Color TabForeColor { get; set; }
    [Browsable(true)]
    public Color SelectedColor { get; set; }
  }
}
