﻿/*
 * Created by SharpDevelop.
 * User: Gamaliel
 * Date: 20/10/2014
 * Time: 01:24 a.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.ComponentModel;

namespace PokeMod.Controls
{
	/// <summary>
	/// Description of NiceListViewItem.
	/// </summary>
	public class NiceListViewItem : System.Windows.Forms.ListViewItem
	{
		public NiceListViewItem() : base()
		{
			
		}
		
		[Category("Appearance")]
		public Color SelectedBackgroundColor { get; set; }
		
	}
}
