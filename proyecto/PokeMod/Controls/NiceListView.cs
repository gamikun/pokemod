﻿/*
 * Created by SharpDevelop.
 * User: Gamaliel
 * Date: 19/10/2014
 * Time: 05:01 p.m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace PokeMod.Controls
{
	/// <summary>
	/// Description of NiceListView.
	/// </summary>
	public class NiceListView : System.Windows.Forms.ListView
	{
		public NiceListView() : base()
		{
			DoubleBuffered = true;
			OwnerDraw = true;
			HeaderBackgroundColor = Color.Transparent;
		}
		
		protected override void OnDrawColumnHeader(
			System.Windows.Forms.DrawListViewColumnHeaderEventArgs e)
		{
			if(HeaderBackgroundColor != Color.Transparent) {
				SolidBrush bgColor = new SolidBrush(HeaderBackgroundColor);
				e.Graphics.FillRectangle(bgColor, e.Bounds);
			}
			e.DrawText();
		}
		
		protected override void OnDrawSubItem(
			System.Windows.Forms.DrawListViewSubItemEventArgs e)
		{
			if(e.ColumnIndex > 0) {
				e.DrawText();
			}
		}
		
		protected override void OnDrawItem(
			System.Windows.Forms.DrawListViewItemEventArgs e)
		{
			SolidBrush brush;
			
			if(this.SelectedItems.Contains(e.Item)) {
				brush = new SolidBrush(SelectedItemBackgroundColor);
				if(e.Item.GetType() == typeof(NiceListViewItem)) {
					NiceListViewItem item = (NiceListViewItem)e.Item;
					if(item.SelectedBackgroundColor != Color.Transparent) {
						brush = new SolidBrush(item.SelectedBackgroundColor);
					}
				}
				e.Item.Bounds.Inflate(1, 100);
				Rectangle rect = new Rectangle(
					e.Bounds.X, e.Bounds.Y,
					Width, e.Bounds.Height
				);
				
				e.Graphics.FillRectangle(brush, rect);
				
			}
			
			if(e.Item.ImageKey != null) {
				int imageIndex = LargeImageList.Images.IndexOfKey("bulbasaur");
				LargeImageList.Draw(e.Graphics, e.Bounds.Location, imageIndex);
			}

			// e.DrawText();
			SolidBrush fontBrush = new SolidBrush(Color.Black);
			e.Graphics.DrawString("Pasado", this.Font, new SolidBrush(Color.Pink), e.Bounds.X + 48, e.Bounds.Y);
			// e.Graphics.DrawLine(new Pen(Color.Blue), 0, 0, 100, 100);
		}
		
		[Category("Appearance")]
		public Color HeaderBackgroundColor { get; set; }
		
		[Category("Appearance")]
		public Color SelectedItemBackgroundColor { get; set; }
		
		[Category("Appearance")]
		public Color HeaderForeColor { get; set; }
		
		[Category("Appearance")]
		public Color SelectedItemForeColor { get; set; }
		
		protected override bool DoubleBuffered {
			get {
				return base.DoubleBuffered;
			}
			set {
				base.DoubleBuffered = value;
			}
		}
	}
}
