﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokeMod {
  class Pokemon{
    private byte id;
    private string nombre;

    public Pokemon(byte id, string nombre) {
      this.id = id;
      this.nombre = nombre;
    }

    public string Nombre {
      get { return this.nombre; }
      set { this.nombre = value; }
    }
  
    public byte ID {
      get { return this.id; }
    }
    
    public readonly static byte
      PIKACHU = 85;
  }
}
