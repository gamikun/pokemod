﻿namespace PokeMod
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
          this.gridProp = new System.Windows.Forms.PropertyGrid();
          this.iconos = new System.Windows.Forms.ImageList(this.components);
          this.gbxPokemon = new System.Windows.Forms.GroupBox();
          this.txtNombre = new System.Windows.Forms.TextBox();
          this.label1 = new System.Windows.Forms.Label();
          this.lvwNombres = new System.Windows.Forms.ListView();
          this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
          this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
          this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
          this.lvwAtaques = new System.Windows.Forms.ListView();
          this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
          this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
          this.tbxCharmander = new System.Windows.Forms.TextBox();
          this.cbxCharmander = new System.Windows.Forms.ComboBox();
          this.label3 = new System.Windows.Forms.Label();
          this.label2 = new System.Windows.Forms.Label();
          this.button3 = new System.Windows.Forms.Button();
          this.panel = new System.Windows.Forms.Panel();
          this.txtTest = new System.Windows.Forms.TextBox();
          this.button4 = new System.Windows.Forms.Button();
          this.buttonSave = new PokeMod.SimpleButton();
          this.buttonOpen = new PokeMod.SimpleButton();
          this.buttonHelp = new PokeMod.SimpleButton();
          this.buttonCloseGame = new PokeMod.SimpleButton();
          this.simpleTabs1 = new PokeMod.Controls.SimpleTabs();
          this.tabPage4 = new System.Windows.Forms.TabPage();
          this.tabPage5 = new System.Windows.Forms.TabPage();
          this.tabPage6 = new System.Windows.Forms.TabPage();
          this.tabPage7 = new System.Windows.Forms.TabPage();
          this.tabPage8 = new System.Windows.Forms.TabPage();
          this.simpleButton1 = new PokeMod.SimpleButton();
          this.gbxPokemon.SuspendLayout();
          this.simpleTabs1.SuspendLayout();
          this.tabPage4.SuspendLayout();
          this.tabPage5.SuspendLayout();
          this.tabPage6.SuspendLayout();
          this.tabPage7.SuspendLayout();
          this.tabPage8.SuspendLayout();
          this.SuspendLayout();
          // 
          // gridProp
          // 
          this.gridProp.CommandsDisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
          this.gridProp.Cursor = System.Windows.Forms.Cursors.Default;
          this.gridProp.HelpBackColor = System.Drawing.Color.Gray;
          this.gridProp.HelpVisible = false;
          this.gridProp.LargeButtons = true;
          this.gridProp.LineColor = System.Drawing.Color.Gray;
          this.gridProp.Location = new System.Drawing.Point(17, 17);
          this.gridProp.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
          this.gridProp.Name = "gridProp";
          this.gridProp.PropertySort = System.Windows.Forms.PropertySort.NoSort;
          this.gridProp.Size = new System.Drawing.Size(553, 170);
          this.gridProp.TabIndex = 0;
          this.gridProp.ToolbarVisible = false;
          this.gridProp.ViewBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.gridProp.ViewForeColor = System.Drawing.Color.White;
          // 
          // iconos
          // 
          this.iconos.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("iconos.ImageStream")));
          this.iconos.TransparentColor = System.Drawing.Color.Transparent;
          this.iconos.Images.SetKeyName(0, "casa.png");
          this.iconos.Images.SetKeyName(1, "letra.png");
          this.iconos.Images.SetKeyName(2, "rayo.png");
          this.iconos.Images.SetKeyName(3, "estrella.png");
          // 
          // gbxPokemon
          // 
          this.gbxPokemon.Controls.Add(this.simpleButton1);
          this.gbxPokemon.Controls.Add(this.txtNombre);
          this.gbxPokemon.Controls.Add(this.label1);
          this.gbxPokemon.Enabled = false;
          this.gbxPokemon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
          this.gbxPokemon.ForeColor = System.Drawing.Color.DarkGray;
          this.gbxPokemon.Location = new System.Drawing.Point(511, 18);
          this.gbxPokemon.Name = "gbxPokemon";
          this.gbxPokemon.Size = new System.Drawing.Size(173, 209);
          this.gbxPokemon.TabIndex = 2;
          this.gbxPokemon.TabStop = false;
          this.gbxPokemon.Text = "Pokemon";
          // 
          // txtNombre
          // 
          this.txtNombre.Location = new System.Drawing.Point(15, 49);
          this.txtNombre.MaxLength = 10;
          this.txtNombre.Name = "txtNombre";
          this.txtNombre.Size = new System.Drawing.Size(142, 26);
          this.txtNombre.TabIndex = 1;
          // 
          // label1
          // 
          this.label1.AutoSize = true;
          this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
          this.label1.Location = new System.Drawing.Point(15, 26);
          this.label1.Name = "label1";
          this.label1.Size = new System.Drawing.Size(72, 20);
          this.label1.TabIndex = 0;
          this.label1.Text = "Nombre:";
          // 
          // lvwNombres
          // 
          this.lvwNombres.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.lvwNombres.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.lvwNombres.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                              this.columnHeader2,
                              this.columnHeader1,
                              this.columnHeader3});
          this.lvwNombres.ForeColor = System.Drawing.Color.WhiteSmoke;
          this.lvwNombres.FullRowSelect = true;
          this.lvwNombres.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
          this.lvwNombres.HideSelection = false;
          this.lvwNombres.LabelEdit = true;
          this.lvwNombres.Location = new System.Drawing.Point(21, 20);
          this.lvwNombres.MultiSelect = false;
          this.lvwNombres.Name = "lvwNombres";
          this.lvwNombres.Size = new System.Drawing.Size(460, 203);
          this.lvwNombres.TabIndex = 0;
          this.lvwNombres.UseCompatibleStateImageBehavior = false;
          this.lvwNombres.View = System.Windows.Forms.View.Details;
          this.lvwNombres.SelectedIndexChanged += new System.EventHandler(this.showPokemon);
          // 
          // columnHeader2
          // 
          this.columnHeader2.Text = "ID";
          // 
          // columnHeader1
          // 
          this.columnHeader1.Text = "Nombre";
          this.columnHeader1.Width = 140;
          // 
          // columnHeader3
          // 
          this.columnHeader3.Text = "Clave";
          this.columnHeader3.Width = 110;
          // 
          // lvwAtaques
          // 
          this.lvwAtaques.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.lvwAtaques.BorderStyle = System.Windows.Forms.BorderStyle.None;
          this.lvwAtaques.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                              this.columnHeader4,
                              this.columnHeader5});
          this.lvwAtaques.ForeColor = System.Drawing.Color.WhiteSmoke;
          this.lvwAtaques.FullRowSelect = true;
          this.lvwAtaques.HideSelection = false;
          this.lvwAtaques.LabelEdit = true;
          this.lvwAtaques.Location = new System.Drawing.Point(23, 17);
          this.lvwAtaques.MultiSelect = false;
          this.lvwAtaques.Name = "lvwAtaques";
          this.lvwAtaques.Size = new System.Drawing.Size(665, 237);
          this.lvwAtaques.TabIndex = 1;
          this.lvwAtaques.UseCompatibleStateImageBehavior = false;
          this.lvwAtaques.View = System.Windows.Forms.View.Details;
          // 
          // columnHeader4
          // 
          this.columnHeader4.Text = "ID";
          // 
          // columnHeader5
          // 
          this.columnHeader5.Text = "Nombre";
          this.columnHeader5.Width = 250;
          // 
          // tbxCharmander
          // 
          this.tbxCharmander.Location = new System.Drawing.Point(382, 22);
          this.tbxCharmander.Name = "tbxCharmander";
          this.tbxCharmander.Size = new System.Drawing.Size(49, 26);
          this.tbxCharmander.TabIndex = 2;
          // 
          // cbxCharmander
          // 
          this.cbxCharmander.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
          this.cbxCharmander.FormattingEnabled = true;
          this.cbxCharmander.Location = new System.Drawing.Point(177, 22);
          this.cbxCharmander.Name = "cbxCharmander";
          this.cbxCharmander.Size = new System.Drawing.Size(150, 28);
          this.cbxCharmander.TabIndex = 1;
          // 
          // label3
          // 
          this.label3.Location = new System.Drawing.Point(333, 25);
          this.label3.Name = "label3";
          this.label3.Size = new System.Drawing.Size(43, 17);
          this.label3.TabIndex = 0;
          this.label3.Text = "al nivel";
          // 
          // label2
          // 
          this.label2.Location = new System.Drawing.Point(31, 25);
          this.label2.Name = "label2";
          this.label2.Size = new System.Drawing.Size(144, 17);
          this.label2.TabIndex = 0;
          this.label2.Text = "Charmander evoluciona en";
          // 
          // button3
          // 
          this.button3.Location = new System.Drawing.Point(473, 157);
          this.button3.Name = "button3";
          this.button3.Size = new System.Drawing.Size(142, 37);
          this.button3.TabIndex = 2;
          this.button3.Text = "mostrar";
          this.button3.UseVisualStyleBackColor = true;
          this.button3.Click += new System.EventHandler(this.Button3Click);
          // 
          // panel
          // 
          this.panel.Location = new System.Drawing.Point(438, 37);
          this.panel.Name = "panel";
          this.panel.Size = new System.Drawing.Size(200, 100);
          this.panel.TabIndex = 1;
          // 
          // txtTest
          // 
          this.txtTest.Location = new System.Drawing.Point(18, 17);
          this.txtTest.Multiline = true;
          this.txtTest.Name = "txtTest";
          this.txtTest.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
          this.txtTest.Size = new System.Drawing.Size(339, 161);
          this.txtTest.TabIndex = 0;
          // 
          // button4
          // 
          this.button4.Location = new System.Drawing.Point(587, 17);
          this.button4.Name = "button4";
          this.button4.Size = new System.Drawing.Size(97, 72);
          this.button4.TabIndex = 2;
          this.button4.Text = "Ver ataques";
          this.button4.UseVisualStyleBackColor = true;
          // 
          // buttonSave
          // 
          this.buttonSave.BackColor = System.Drawing.Color.SlateGray;
          this.buttonSave.DownColor = System.Drawing.Color.DarkOliveGreen;
          this.buttonSave.Enabled = false;
          this.buttonSave.Font = new System.Drawing.Font("Century Gothic", 15F);
          this.buttonSave.ForeColor = System.Drawing.Color.White;
          this.buttonSave.HoverColor = System.Drawing.Color.OliveDrab;
          this.buttonSave.Location = new System.Drawing.Point(122, 12);
          this.buttonSave.Name = "buttonSave";
          this.buttonSave.Size = new System.Drawing.Size(147, 33);
          this.buttonSave.TabIndex = 5;
          this.buttonSave.Text = "guardar";
          this.buttonSave.Click += new System.EventHandler(this.saveGame);
          // 
          // buttonOpen
          // 
          this.buttonOpen.BackColor = System.Drawing.Color.SlateGray;
          this.buttonOpen.DownColor = System.Drawing.Color.DarkSlateBlue;
          this.buttonOpen.Font = new System.Drawing.Font("Century Gothic", 15F);
          this.buttonOpen.ForeColor = System.Drawing.Color.White;
          this.buttonOpen.HoverColor = System.Drawing.Color.SlateBlue;
          this.buttonOpen.Location = new System.Drawing.Point(12, 12);
          this.buttonOpen.Name = "buttonOpen";
          this.buttonOpen.Size = new System.Drawing.Size(96, 33);
          this.buttonOpen.TabIndex = 6;
          this.buttonOpen.Text = "abrir";
          this.buttonOpen.Click += new System.EventHandler(this.openGame);
          // 
          // buttonHelp
          // 
          this.buttonHelp.BackColor = System.Drawing.Color.SlateGray;
          this.buttonHelp.DownColor = System.Drawing.Color.Peru;
          this.buttonHelp.Font = new System.Drawing.Font("Century Gothic", 15F);
          this.buttonHelp.ForeColor = System.Drawing.Color.White;
          this.buttonHelp.HoverColor = System.Drawing.Color.SandyBrown;
          this.buttonHelp.Location = new System.Drawing.Point(701, 12);
          this.buttonHelp.Name = "buttonHelp";
          this.buttonHelp.Size = new System.Drawing.Size(31, 33);
          this.buttonHelp.TabIndex = 7;
          this.buttonHelp.Text = "?";
          this.buttonHelp.Click += new System.EventHandler(this.showHelp);
          // 
          // buttonCloseGame
          // 
          this.buttonCloseGame.BackColor = System.Drawing.Color.SlateGray;
          this.buttonCloseGame.DownColor = System.Drawing.Color.DarkRed;
          this.buttonCloseGame.Enabled = false;
          this.buttonCloseGame.Font = new System.Drawing.Font("Century Gothic", 15F);
          this.buttonCloseGame.ForeColor = System.Drawing.Color.White;
          this.buttonCloseGame.HoverColor = System.Drawing.Color.Brown;
          this.buttonCloseGame.Location = new System.Drawing.Point(565, 12);
          this.buttonCloseGame.Name = "buttonCloseGame";
          this.buttonCloseGame.Size = new System.Drawing.Size(120, 33);
          this.buttonCloseGame.TabIndex = 9;
          this.buttonCloseGame.Text = "cerrar";
          // 
          // simpleTabs1
          // 
          this.simpleTabs1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
          this.simpleTabs1.Controls.Add(this.tabPage4);
          this.simpleTabs1.Controls.Add(this.tabPage5);
          this.simpleTabs1.Controls.Add(this.tabPage6);
          this.simpleTabs1.Controls.Add(this.tabPage7);
          this.simpleTabs1.Controls.Add(this.tabPage8);
          this.simpleTabs1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
          this.simpleTabs1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
          this.simpleTabs1.ItemSize = new System.Drawing.Size(100, 30);
          this.simpleTabs1.Location = new System.Drawing.Point(10, 61);
          this.simpleTabs1.Margin = new System.Windows.Forms.Padding(0);
          this.simpleTabs1.Multiline = true;
          this.simpleTabs1.Name = "simpleTabs1";
          this.simpleTabs1.Padding = new System.Drawing.Point(0, 0);
          this.simpleTabs1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.simpleTabs1.SelectedIndex = 0;
          this.simpleTabs1.Size = new System.Drawing.Size(727, 313);
          this.simpleTabs1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
          this.simpleTabs1.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(84)))));
          this.simpleTabs1.TabForeColor = System.Drawing.Color.White;
          this.simpleTabs1.TabIndex = 10;
          // 
          // tabPage4
          // 
          this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.tabPage4.Controls.Add(this.button4);
          this.tabPage4.Controls.Add(this.gridProp);
          this.tabPage4.Location = new System.Drawing.Point(4, 34);
          this.tabPage4.Margin = new System.Windows.Forms.Padding(0);
          this.tabPage4.Name = "tabPage4";
          this.tabPage4.Size = new System.Drawing.Size(719, 275);
          this.tabPage4.TabIndex = 0;
          this.tabPage4.Text = "inicio";
          // 
          // tabPage5
          // 
          this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.tabPage5.Controls.Add(this.gbxPokemon);
          this.tabPage5.Controls.Add(this.lvwNombres);
          this.tabPage5.Location = new System.Drawing.Point(4, 34);
          this.tabPage5.Margin = new System.Windows.Forms.Padding(0);
          this.tabPage5.Name = "tabPage5";
          this.tabPage5.Size = new System.Drawing.Size(719, 275);
          this.tabPage5.TabIndex = 1;
          this.tabPage5.Text = "nombres";
          // 
          // tabPage6
          // 
          this.tabPage6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.tabPage6.Controls.Add(this.lvwAtaques);
          this.tabPage6.Location = new System.Drawing.Point(4, 34);
          this.tabPage6.Margin = new System.Windows.Forms.Padding(0);
          this.tabPage6.Name = "tabPage6";
          this.tabPage6.Size = new System.Drawing.Size(719, 275);
          this.tabPage6.TabIndex = 2;
          this.tabPage6.Text = "ataques";
          // 
          // tabPage7
          // 
          this.tabPage7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.tabPage7.Controls.Add(this.tbxCharmander);
          this.tabPage7.Controls.Add(this.cbxCharmander);
          this.tabPage7.Controls.Add(this.label2);
          this.tabPage7.Controls.Add(this.label3);
          this.tabPage7.Location = new System.Drawing.Point(4, 34);
          this.tabPage7.Margin = new System.Windows.Forms.Padding(0);
          this.tabPage7.Name = "tabPage7";
          this.tabPage7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
          this.tabPage7.Size = new System.Drawing.Size(719, 275);
          this.tabPage7.TabIndex = 3;
          this.tabPage7.Text = "evoluciones";
          // 
          // tabPage8
          // 
          this.tabPage8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(56)))), ((int)(((byte)(56)))));
          this.tabPage8.Controls.Add(this.button3);
          this.tabPage8.Controls.Add(this.txtTest);
          this.tabPage8.Controls.Add(this.panel);
          this.tabPage8.Location = new System.Drawing.Point(4, 34);
          this.tabPage8.Margin = new System.Windows.Forms.Padding(0);
          this.tabPage8.Name = "tabPage8";
          this.tabPage8.Size = new System.Drawing.Size(719, 275);
          this.tabPage8.TabIndex = 4;
          this.tabPage8.Text = "testeo";
          // 
          // simpleButton1
          // 
          this.simpleButton1.BackColor = System.Drawing.Color.SlateGray;
          this.simpleButton1.DownColor = System.Drawing.Color.DarkOliveGreen;
          this.simpleButton1.Enabled = false;
          this.simpleButton1.Font = new System.Drawing.Font("Century Gothic", 15F);
          this.simpleButton1.ForeColor = System.Drawing.Color.White;
          this.simpleButton1.HoverColor = System.Drawing.Color.OliveDrab;
          this.simpleButton1.Location = new System.Drawing.Point(26, 160);
          this.simpleButton1.Name = "simpleButton1";
          this.simpleButton1.Size = new System.Drawing.Size(125, 33);
          this.simpleButton1.TabIndex = 11;
          this.simpleButton1.Text = "guardar";
          this.simpleButton1.Click += new System.EventHandler(this.GuardarNombre);
          // 
          // MainForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
          this.ClientSize = new System.Drawing.Size(747, 383);
          this.Controls.Add(this.simpleTabs1);
          this.Controls.Add(this.buttonCloseGame);
          this.Controls.Add(this.buttonHelp);
          this.Controls.Add(this.buttonOpen);
          this.Controls.Add(this.buttonSave);
          this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
          this.MaximizeBox = false;
          this.Name = "MainForm";
          this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
          this.Text = "PokeMod 0.2b";
          this.Load += new System.EventHandler(this.MainForm_Load);
          this.gbxPokemon.ResumeLayout(false);
          this.gbxPokemon.PerformLayout();
          this.simpleTabs1.ResumeLayout(false);
          this.tabPage4.ResumeLayout(false);
          this.tabPage5.ResumeLayout(false);
          this.tabPage6.ResumeLayout(false);
          this.tabPage7.ResumeLayout(false);
          this.tabPage7.PerformLayout();
          this.tabPage8.ResumeLayout(false);
          this.tabPage8.PerformLayout();
          this.ResumeLayout(false);
        }
        private PokeMod.SimpleButton simpleButton1;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private PokeMod.Controls.SimpleTabs simpleTabs1;
        private PokeMod.SimpleButton buttonCloseGame;
        private PokeMod.SimpleButton buttonHelp;
        private PokeMod.SimpleButton buttonOpen;
        private PokeMod.SimpleButton buttonSave;
        private System.Windows.Forms.ImageList iconos;
        private System.Windows.Forms.TextBox tbxCharmander;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxCharmander;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ListView lvwAtaques;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.TextBox txtTest;
        private System.Windows.Forms.GroupBox gbxPokemon;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView lvwNombres;

        #endregion

        private System.Windows.Forms.PropertyGrid gridProp;
    }
}

